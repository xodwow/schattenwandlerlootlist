## Interface: 11305
## Title: Schattenwandler Loot List
## Notes: Lists in the tooltip which classes have an item in the loot list.
## Author: Xod
## Version: 0.46
## X-Category: Inventory
## SavedVariables: SchattenwandlerLootListData

embeds.xml
main.lua
import.lua
cmd.lua
data.lua
creates.lua
