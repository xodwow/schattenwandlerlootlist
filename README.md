# SchattenwandlerLootList

Dieses Addon macht die Daten der `<Schattenwandler>` Main-Raid Lootliste direkt im Spiel verfügbar.
Es ist dafür gedacht, um einen schnellen Überblick zu erhalten und bietet nur eine Hilfestellung zur Google Docs Lootliste.

# [Download](https://gitlab.com/xodwow/schattenwandlerlootlist/builds/artifacts/master/raw/SchattenwandlerLootList.zip?job=build)

## Änderungen
* v0.46 Zwei lua-Fehler behoben
* v0.45 Earthshatter T3-Mapping repariert
* v0.44 Atiesh-Support und neue Naxx-Liste
* v0.43 Naxxramas Update
* v0.42 Loot-Nachrichten im Chat werden nicht mehr verwendet (nur noch inspect und sync)
* v0.41 Itemtabelle aktualisiert
* v0.40 Waffen-Regeln ergänzt und Fehler bei "Blessed Qiraji Pugio" behoben
* v0.39 Offis (ab Schattendämon) können jetzt auch Daten synchronisieren
* v0.38 Behebt sporadische `bad argument #1 to 'gsub'` Fehler
* v0.37 Loot-Infos werden auch bei geklickten Item-Links angezeigt
* v0.36 Erste Testversion mit Daten-Synchronisation
* v0.35 Enthält die Spieler- und Loot-Rohdaten aus dem Google Docs

## Tooltip
Im Tooltip werden pro Spezialisierung alle Spieler aufgelistet, die in dem dazugehörigem Slot noch kein "X" haben.
Haben alle Spieler der jeweiligen Spezialisierung ein "X" in dem Slot, wird "-" angezeigt.
Hinter dem jeweiligen Namen wird ggf. der Trial-Status angezeigt und wie viele "X" insgesamt vorhanden sind.
Wenn man in einem Raid ist, werden Spieler, die nicht im Raid sind, automatisch grau angezeigt.

![Screenshot eines Tooltips der zeigt, welche Spieler das Item als erstes bekommen](Screenshot_20200830_172751.png)

## Daten
Wer welche Items bekommen will bzw. besitzt sind im Addon auf Basis des Google Docs hinterlegt ([teilautomatisiert](exporter/)) und wird automatisch im Spiel aktualisiert (durch eine Synchronisation und automatisches Betrachten).

## Erweitere Features
Es gibt ein paar Befehle, mit denen sich die vom Addon gespeicherten Daten ausgeben lassen.

### Tabellarische Übersicht (`/swll table`)
Zeigt in kompakten Übersicht alle Main-Raider an und ob sie in einem Slot ein "X" haben.

![Screenshot der `/swll table` Ausgabe](Screenshot_20200815_163020.png)

### Spieler-Übersicht (`/swll <Spielername>`)
Zeigt die lokal gespeicherten Informationen über einen bestimmten Spieler an.

![Screenshot der `/swll Xod` Ausgabe](Screenshot_20200816_140017.png)

### Item-Übersicht (`/swll <Itemlink>`)
Zeigt an wer wann einen bestimmten Gegenstand bekommen hat.

![Screenshot der `/swll [Head of Nefarian]` Ausgabe](Screenshot_20200816_140703.png)

### Zeit-Übersicht (`/swll <Jahr>-<Monat>-<Tag>`)
Zeigt an welche items zu einem bestimmten Datum gelootet wurden, zum Beispiel `/swll 2020-08` oder `/swll 2020-07-16`.

![Screenshot der `/swll 2020-05-10` Ausgabe](Screenshot_20200514_175701.png)

### Datenimport
Man kann jederzeit eine aktualisierte [import.lua](https://swll-updater.herokuapp.com/export) auf Basis des Google Docs herunterladen und damit die originale `import.lua` im `SchattenwandlerLootList`-Verzeichnis ersetzen.
Um die neuen Daten anschließend an die Gilde zu übertragen, muss man anschließend im Spiel das Interface neu laden und dann mit `/swll sync` die Daten senden.
