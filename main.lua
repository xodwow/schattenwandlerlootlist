-- Schattenwandler Loot List
SWLL = LibStub("AceAddon-3.0"):NewAddon("SchattenwandlerLootList", "AceComm-3.0", "AceEvent-3.0", "AceSerializer-3.0")
local self = SWLL

local GUILD_RANK_TRIAL = 5
local GUILD_RANK_OFFICER = 2

local SWLL_RANK_RAIDER = 1
local SWLL_RANK_TRIAL = 2
local SWLL_RANK_UNKNOWN = 3

self.SLOT = { EMPTY = 0, FILLED = 1, NON_MAINRAID = 2 }

self.printWarnings = UnitName("player") == "Xod" or UnitName("player") == "Swenchants"
self.playerRanks = {}
self.allowSyncFrom = {
  ["Xod"] = true
}
self.userInRaid = {}

function self:OnEnable()
  self:RAID_ROSTER_UPDATE()
  self:initData()

  self:RegisterEvent("INSPECT_READY")
  self:RegisterEvent("RAID_ROSTER_UPDATE")
  self:RegisterComm("SWLL")

  -- delay guild rank init and re-scan raid, because otherwise some players are missing :(
  C_Timer.After(10, function()
    self:RAID_ROSTER_UPDATE()
    self:RegisterEvent("GUILD_ROSTER_UPDATE")
    GuildRoster()
  end)

  C_Timer.After(15, function() self:autoInspect() end)

  GameTooltip:HookScript("OnTooltipSetItem", self.addInfoToTooltip)
  ItemRefTooltip:HookScript("OnTooltipSetItem", self.addInfoToTooltip)
end

function self:RAID_ROSTER_UPDATE()
  if table.getn(self.userInRaid) > 0 then self.userInRaid = {} end
  for raidIndex=1, MAX_RAID_MEMBERS do
    local unit = "raid" .. raidIndex
    local playerName = UnitName(unit)
    if playerName then
      self.userInRaid[playerName] = true
    end
  end
end

function self:INSPECT_READY(event, guid)
  local _, playerClass, _, _, _, playerName = GetPlayerInfoByGUID(guid)
  if not playerName or not self.playerRanks[playerName] then return end
  self.data.lastInspected[playerName] = time()
  local unit = self:findUnit(playerName)
  if not unit then return end
  for i = 1, 18 do
    local itemID = GetInventoryItemID(unit, i)
    if itemID and self:needsForMainList(playerName, itemID) then
      self:saveEquipped(playerName, playerClass, itemID)
    end
  end
end

function self:GUILD_ROSTER_UPDATE()
  local numTotal, numOnline = GetNumGuildMembers()
  for guildIndex=1, numTotal do
    local playerNameWithRealm, rankName, guildRankIndex = GetGuildRosterInfo(guildIndex)
    -- is sometimes nil?
    if not playerNameWithRealm then break end
    local playerName = string.gsub(playerNameWithRealm, "%-[^|]+", "")
    if guildRankIndex < GUILD_RANK_TRIAL then
      self.playerRanks[playerName] = SWLL_RANK_RAIDER
    elseif guildRankIndex == GUILD_RANK_TRIAL then
      self.playerRanks[playerName] = SWLL_RANK_TRIAL
    else
      self.playerRanks[playerName] = SWLL_RANK_UNKNOWN
    end
    if guildRankIndex <= GUILD_RANK_OFFICER then
      self.allowSyncFrom[playerName] = true
    end
  end
end

function self:OnCommReceived(prefix, message, distribution, sender)
  local valid, data = self:Deserialize(message)
  if not valid then
    self:printf("Received syntactically invalid data from %s", sender)
    return
  end
  if data.version ~= 4 then
    self:printf("Received data with incompatible version (%s) from %s", data.version, sender)
    return
  end
  if not self.allowSyncFrom[sender] then
    self:printf("Discarding data from %s (not allowed to sync)", sender)
    return
  end
  if data.timestamp <= self.IMPORT.timestamp then
    self:printf("Skipping update from %s (already up to date)", sender)
    return
  end

  self:printf("Received loot data at timestamp %s from %s", data.timestamp, sender)
  self.data.import = data
  self:importData()
end

function self:findUnit(playerName)
  if playerName == UnitName("target") then return "target" end
  for raidIndex=1, MAX_RAID_MEMBERS do
    local unit = "raid" .. raidIndex
    local raidPlayerName = UnitName(unit)
    if raidPlayerName == playerName then
      return unit
    end
  end
end

function self:classColored(playerName, playerClass, showRank)
  if showRank == nil then showRank = true end
  local suffix
  if not showRank or self.playerRanks[playerName] == SWLL_RANK_RAIDER then
    suffix = ""
  elseif self.playerRanks[playerName] == SWLL_RANK_TRIAL then
    suffix = " (trial)"
  else
    suffix = " (?)"
  end
  if playerClass then
    return "|c" .. RAID_CLASS_COLORS[playerClass].colorStr .. playerName .. "|r" .. suffix
  else
    return "|cff888888" .. playerName .. "|r" .. suffix
  end
end

function self.addInfoToTooltip(tooltip)
  local itemName, link = tooltip:GetItem()
  if not link then return end
  local itemID = GetItemInfoInstant(link)
  if IsAltKeyDown() then
    self:showLootLogTooltip(tooltip, itemID)
  else
    self:showMissingTooltip(tooltip, itemID)
  end
  tooltip:Show()
end

function self:showMissingTooltip(tooltip, itemID)
  local isInRaid = IsInRaid()
  for _, playerInfo in pairs(self.IMPORT.players) do
    local playerName = playerInfo.name
    local mainListStr
    local altListStr
    if self:needsForMainList(playerName, itemID) then
      local available = not isInRaid or self.userInRaid[playerName]
      local itemCount = self:playerItemCount(playerName)
      local coloredName = self:classColored(playerName, available and playerInfo.class or nil)
      mainListStr = (mainListStr or "") .. " " .. string.format("%s (%d)", coloredName, itemCount)
    elseif self:needsForAlternativeList(playerName, itemID) then
      local available = not isInRaid or self.userInRaid[playerName]
      local coloredName = self:classColored(playerName, available and playerInfo.class or nil)
      altListStr = (altListStr or "") .. " " .. string.format("%s", coloredName)
    end
    if mainListStr then
      tooltip:AddLine("Main list:" .. mainListStr, 1, 1, 1, true)
    end
    if altListStr then
      tooltip:AddLine("Alternative list:" .. altListStr, 1, 1, 1, true)
    end
  end
end

function self:showLootLogTooltip(tooltip, itemID)
  for playerName, items in pairs(SchattenwandlerLootListData.items) do
    if items[itemID] then
      local playerClass = self.PLAYER_INFO[playerName] and self.PLAYER_INFO[playerName].class
      for _, looted in pairs(items[itemID]) do
        tooltip:AddLine(self:classColored(playerName, playerClass, false) .. " " .. looted)
      end
    end
  end
end
