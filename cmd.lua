local self = SWLL

local CLASS_ORDER = {
  "DRUID",
  "HUNTER",
  "MAGE",
  "PRIEST",
  "ROGUE",
  "SHAMAN",
  "WARLOCK",
  "WARRIOR",
}

function self.slashCommand(message, editbox)
  if message:find("|Hitem:%d+:") then
    return self:listItem(tonumber(message:match("|Hitem:(%d+):")))
  elseif self.data and self.data.items and self.data.items[self:formatPlayerName(message)] then
    return self:listPlayer(self:formatPlayerName(message))
  elseif message:match("^[%dT:\\-]+$") then
    return self:listDate(message)
  elseif message == "inspect" then
    self:startInspectAll("manual")
    return
  elseif message == "table" then
    self:printTable()
    return
  elseif message == "sync" then
    self:sync()
    return
  end

  self:printf("Usage")
  self:printf("/swll %s: Print the loot history for a specific item", "[itemLink]")
  self:printf("/swll %s: Print the loot history for a specific player", "playerName")
  self:printf("/swll %s: Print the loot history for a specific date (e.g. 2020-12-31)", "date")
  self:printf("/swll %s: Inspect the whole raid the add missing items to the loot database", "inspect")
  self:printf("/swll %s: Show an overview table similar to the online loot sheet", "table")
end

function self:listItem(itemID)
  local itemLink = self:getItemLink(itemID)
  self:printf("Loot history of %s:", itemLink)
  for playerName, items in pairs(self.data.items) do
    local playerClass = self.PLAYER_INFO[playerName].class
    if items[itemID] then
      for _, looted in pairs(items[itemID]) do
        self:printf("%s: %s", self:classColored(playerName, playerClass), looted)
      end
    end
  end
end

function self:listPlayer(playerName)
  local playerClass = self.PLAYER_INFO[playerName].class
  local itemsOfPlayer = self.data.items[playerName]
  if not itemsOfPlayer then
    self:printf("Player %s is unknown or got no items.", self:classColored(playerName, playerClass))
    return
  end

  self:printf("Player %s:", self:classColored(playerName, playerClass))
  local slots = self.PLAYER_INFO[playerName] and self.PLAYER_INFO[playerName].slots
  local lines = {}
  for itemID, items in pairs(itemsOfPlayer) do
    local found = false
    local itemLink = self:getItemLink(itemID)
    for _, looted in pairs(items) do
      for slotIndex, slotData in pairs(slots) do
        local slotName = self.INDEX_TO_SLOT[slotIndex]
        local hasItem, hasAlternative, bestItems, optionalItems = unpack(slotData)
        if self:tableIncludes(bestItems, itemID) then
          found = true
          table.insert(lines, self:format("%s: %s (Primary %s)", looted, itemLink, slotName))
        elseif self:tableIncludes(optionalItems, itemID) then
          found = true
          table.insert(lines, self:format("%s: %s (Alternative %s)", looted, itemLink, slotName))
        end
      end
    end
    if not found then
      table.insert(lines, self:format("%s: %s", items[1], itemLink))
    end
  end
  table.sort(lines, function (a, b) return string.lower(a) < string.lower(b) end)
  for _, line in pairs(lines) do
    print(line)
  end
end

function self:concat(tbl)
  local str = ""
  local sep = ""
  for _, s in pairs(tbl) do
    str = str .. sep .. s
    sep = ", "
  end
end

function self:formatPlayerName(playerName)
  return string.upper(string.sub(playerName, 1, 1)) .. string.lower(string.sub(playerName, 2))
end

function self:listDate(date)
  self:printf("Inspect history for %s:", date)
  local lines = {}
  for playerName, playerItems in pairs(self.data.items) do
    local playerClass = self.PLAYER_INFO[playerName].class
    for itemID, list in pairs(playerItems) do
      for _, looted in pairs(list) do
        if string.find(looted, date, 1, true) then
          local itemLink = self:getItemLink(itemID)
          table.insert(lines, self:format("%s: %s -> %s", looted, itemLink, self:classColored(playerName, playerClass)))
        end
      end
    end
  end
  table.sort(lines, function (a, b) return string.lower(a) < string.lower(b) end)
  for _, line in pairs(lines) do
    print(line)
  end
end

function self:sync()
  self:SendCommMessage("SWLL", self:Serialize(self.IMPORT), "GUILD")
end

function self:autoInspect()
  C_Timer.After(60, function() self:autoInspect() end)
  if self.inspecting or InCombatLockdown() or not IsInRaid() then return end
  self:startInspectAll('auto')
end

function self:startInspectAll(mode)
  self.inspecting = { index = 0, mode = mode }
  self:continueInspectAll()
end

function self:continueInspectAll()
  if not self.inspecting or self.inspecting.index > MAX_RAID_MEMBERS then
    if self.inspecting.mode == 'manual' then
      self:printf("Inspecting finished")
    end
    self.inspecting = nil
    ClearInspectPlayer()
    return
  end

  self.inspecting.index = self.inspecting.index + 1
  local unit = "raid" .. self.inspecting.index
  local playerName = UnitName(unit)
  local playerInfo = playerName and self.PLAYER_INFO[playerName]
  local playerClass = playerInfo and playerInfo.class
  if playerClass and self:shouldInspect(playerName) then
    local coloredName = self:classColored(playerName, playerClass, false)
    if CheckInteractDistance(unit, 1) and CanInspect(unit) then
      if self.inspecting.mode == 'manual' then
        self:printf("Inspecting %s (%s)...", coloredName, unit)
      end
      NotifyInspect(unit)
      C_Timer.After(5, function() self:continueInspectAll() end)
      return
    elseif self.inspecting.mode == 'manual' then
      self:printf("Can't inspect %s, out of range", coloredName)
    end
  end

  self:continueInspectAll()
end

function self:shouldInspect(playerName)
  if self.inspecting.mode == 'manual' then
    return true
  else
    return time() - (self.data.lastInspected[playerName] or 0) > 3600
  end
end

function self:printTable()
  local text = ""
  local header = ""
  for _, slot in pairs(self.INDEX_TO_SLOT) do
    header = header .. self:padLeft(10, slot)
  end
  local lines = {}

  for _, class in pairs(CLASS_ORDER) do
    table.insert(lines, self:padRight(30, self:classColored(class, class, false)) .. header)
    local linesPerClass = {}
    for playerName, playerInfo in pairs(self.PLAYER_INFO) do
      if class == playerInfo.class then
        local coloredName = self:classColored(playerName, class, false)
        local line = self:padRight(25, coloredName) .. self:padRight(5, string.format("(%d)", self:playerItemCount(playerName)))
        for _, slot in pairs(self.INDEX_TO_SLOT) do
          line = line .. self:padLeft(10, self:slotCharacter(self:exactSlotState(playerName, slot)))
        end
        table.insert(linesPerClass, line)
      end
    end
    table.sort(linesPerClass)
    for _, line in pairs(linesPerClass) do
      table.insert(lines, line)
    end
    table.insert(lines, "")
  end

  self:showEditBox(table.concat(lines, "\n"))
end

function self:slotCharacter(state)
  if state == self.SLOT.FILLED then
    return "X"
  elseif state == self.SLOT.NON_MAINRAID then
    return "Q"
  else
    return " "
  end
end

function self:padLeft(targetLength, str)
  local length = strlenutf8(str)
  if length < targetLength then
    return string.rep(" ", targetLength - length) .. str
  end
  return str
end

function self:padRight(targetLength, str)
  local length = strlenutf8(str)
  if length < targetLength then
    return str .. string.rep(" ", targetLength - length)
  end
  return str
end

-- based on https://www.wowinterface.com/forums/showpost.php?p=323901&postcount=2
function self:showEditBox(text)
  if not SWLLEditBox then
    local f = CreateFrame("Frame", "SWLLEditBox", UIParent, "DialogBoxFrame")
    f:SetPoint("CENTER")
    f:SetSize(1400, 500)

    f:SetBackdrop({
      bgFile = "Interface\\DialogFrame\\UI-DialogBox-Background",
      edgeFile = "Interface\\PVPFrame\\UI-Character-PVP-Highlight",
      edgeSize = 16,
      insets = { left = 8, right = 6, top = 8, bottom = 8 },
    })
    f:SetBackdropBorderColor(0, .44, .87, .5)

    -- Movable
    f:SetMovable(true)
    f:SetClampedToScreen(true)
    f:SetScript("OnMouseDown", function(self, button)
      if button == "LeftButton" then
        self:StartMoving()
      end
    end)
    f:SetScript("OnMouseUp", f.StopMovingOrSizing)

    -- ScrollFrame
    local sf = CreateFrame("ScrollFrame", "SWLLEditBoxScrollFrame", SWLLEditBox, "UIPanelScrollFrameTemplate")
    sf:SetPoint("LEFT", 16, 0)
    sf:SetPoint("RIGHT", -32, 0)
    sf:SetPoint("TOP", 0, -16)
    sf:SetPoint("BOTTOM", SWLLEditBoxButton, "TOP", 0, 0)

    -- EditBox
    local eb = CreateFrame("EditBox", "SWLLEditBoxEditBox", SWLLEditBoxScrollFrame)
    eb:SetSize(sf:GetSize())
    eb:SetMultiLine(true)
    eb:SetAutoFocus(false)
    eb:SetFont("Interface\\AddOns\\SchattenwandlerLootList\\media\\fonts\\Inconsolata.ttf", 12)
    eb:SetScript("OnEscapePressed", function() f:Hide() end)
    sf:SetScrollChild(eb)

    -- Resizable
    f:SetResizable(true)
    f:SetMinResize(150, 100)

    local rb = CreateFrame("Button", "SWLLEditBoxResizeButton", SWLLEditBox)
    rb:SetPoint("BOTTOMRIGHT", -6, 7)
    rb:SetSize(16, 16)

    rb:SetNormalTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Up")
    rb:SetHighlightTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Highlight")
    rb:SetPushedTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Down")

    rb:SetScript("OnMouseDown", function(self, button)
      if button == "LeftButton" then
        f:StartSizing("BOTTOMRIGHT")
        self:GetHighlightTexture():Hide() -- more noticeable
      end
    end)
    rb:SetScript("OnMouseUp", function(self, button)
      f:StopMovingOrSizing()
      self:GetHighlightTexture():Show()
      eb:SetWidth(sf:GetWidth())
    end)
    f:Show()
  end

  SWLLEditBoxEditBox:SetText(text or "")
  SWLLEditBox:Show()
end

function self:printf(formatString, ...)
  print(self:format(formatString, ...))
end

function self:format(formatString, arg1, arg2, arg3)
  arg1 = arg1 and "|cfff4ffba" .. arg1 .. "|r|cffe9ff78"
  arg2 = arg2 and "|cfff4ffba" .. arg2 .. "|r|cffe9ff78"
  arg3 = arg3 and "|cfff4ffba" .. arg3 .. "|r|cffe9ff78"
  return "|cffe9ff78SWLL: " .. string.format(formatString, arg1, arg2, arg3) .. "|r"
end

SLASH_SWLL1 = "/swll"
SlashCmdList["SWLL"] = self.slashCommand
