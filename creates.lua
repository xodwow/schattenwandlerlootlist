-- Wowhead snippet to generate questitem -> reward data:
-- let str = `-- ${g_pageInfo.name}\n`; str += `[${g_pageInfo.typeId}] = {\n`; let links = Array.from(document.querySelectorAll('#tab-objective-of a')).map(a => /item=([0-9]+)\/([^\/]+)/.exec(a.href)).filter(Boolean); links.forEach(([_, id, name]) => str += `  ${id}, -- ${name.replace(/-/g, ' ').split(' ').map(w => (w === 'the' || w === 'of') ? w : w[0].toUpperCase() + w.substr(1)).join(' ')}\n`); str += '},\n'; console.log(str)

local self = SWLL

self.CREATES = {
  -- Eye of Sulfuras
  [17204] = {
    17182, -- Sulfuras, Hand of Ragnaros
  },
  -- Head of Onyxia
  [18422] = {
    18403, -- Dragonslayer's Signet
    18404, -- Onyxia Tooth Pendant
    18406, -- Onyxia Blood Talisman
  },
  -- Bindings of the Windseeker (Geddon)
  [18563] = {
    19019, -- Thunderfury, Blessed Blade of the Windseeker
  },
  -- Bindings of the Windseeker (Garr)
  [18564] = {
    19019, -- Thunderfury, Blessed Blade of the Windseeker
  },
  -- Head of Nefarian
  [19002] = {
    19366, -- Master Dragonslayer's Orb
    19383, -- Master Dragonslayer's Medallion
    19384, -- Master Dragonslayer's Ring
  },
  -- Heart of Hakkar
  [19802] = {
    19948, -- Zandalarian Hero Badge
    19949, -- Zandalarian Hero Medallion
    19950, -- Zandalarian Hero Charm
  },
  -- Primal Hakkari Armsplint
  [19717] = {
    19830, -- Zandalar Augurs Bracers
    19836, -- Zandalar Madcaps Bracers
    19824, -- Zandalar Vindicators Armguards
  },
  -- Primal Hakkari Tabard
  [19722] = {
    19828, -- Zandalar Augurs Hauberk
    19825, -- Zandalar Freethinkers Breastplate
    19838, -- Zandalar Haruspexs Tunic
  },
  -- Head of the Broodlord Lashlayer
  [20383] = {
    21520, -- Ravencrest's Legacy
    21521, -- Runesword of the Red
    21522, -- Shadowsong's Sorrow
    21523, -- Fang of Korialstrasz
  },
  -- Qiraji Magisterial Ring
  [20884] = {
    21408, -- Band of Unending Life
    21414, -- Band of Vaulted Secrets
    21396, -- Ring of Eternal Justice
    21399, -- Ring of the Gathering Storm
    21393, -- Signet of Unyielding Strength
  },
  -- Qiraji Martial Drape
  [20885] = {
    21406, -- Cloak of Veiled Shadows
    21394, -- Drape of Unyielding Strength
    21415, -- Drape of Vaulted Secrets
    21412, -- Shroud of Infinite Wisdom
  },
  -- Qiraji Spiked Hilt
  [20886] = {
    21392, -- Sickle of Unyielding Strength
    21395, -- Blade of Eternal Justice
    21398, -- Hammer of the Gathering Storm
    21401, -- Scythe of the Unseen Path
    21404, -- Dagger of Veiled Shadows
  },
  -- Qiraji Ceremonial Ring
  [20888] = {
    21405, -- Band of Veiled Shadows
    21411, -- Ring of Infinite Wisdom
    21417, -- Ring of Unspoken Names
    21402, -- Signet of the Unseen Path
  },
  -- Qiraji Regal Drape
  [20889] = {
    21397, -- Cape of Eternal Justice
    21400, -- Cloak of the Gathering Storm
    21403, -- Cloak of the Unseen Path
    21409, -- Cloak of Unending Life
    21418, -- Shroud of Unspoken Names
  },
  -- Qiraji Ornate Hilt
  [20890] = {
    21407, -- Mace of Unending Life
    21410, -- Gavel of Infinite Wisdom
    21413, -- Blade of Vaulted Secrets
    21416, -- Kris of Unspoken Names
  },
  -- Vek'nilash's Circlet
  [20926] = {
    21329, -- Conquerors Crown
    21337, -- Doomcallers Circlet
    21347, -- Enigma Circlet
    21348, -- Tiara of the Oracle
  },
  -- Ouro's Intact Hide
  [20927] = {
    21332, -- Conquerors Legguards
    21362, -- Deathdealers Leggings
    21346, -- Enigma Leggings
    21352, -- Trousers of the Oracle
  },
  -- Qiraji Bindings of Command
  [20928] = {
    21333, -- Conquerors Greaves
    21330, -- Conquerors Spaulders
    21359, -- Deathdealers Boots
    21361, -- Deathdealers Spaulders
    21349, -- Footwraps of the Oracle
    21350, -- Mantle of the Oracle
    21365, -- Strikers Footguards
    21367, -- Strikers Pauldrons
  },
  -- Carapace of the Old God
  [20929] = {
    21389, -- Avengers Breastplate
    21331, -- Conquerors Breastplate
    21364, -- Deathdealers Vest
    21374, -- Stormcallers Hauberk
    21370, -- Strikers Hauberk
  },
  -- Vek'lor's Diadem
  [20930] = {
    21387, -- Avengers Crown
    21360, -- Deathdealers Helm
    21353, -- Genesis Helm
    21372, -- Stormcallers Diadem
    21366, -- Strikers Diadem
  },
  -- Skin of the Great Sandworm
  [20931] = {
    21390, -- Avengers Legguards
    21336, -- Doomcallers Trousers
    21356, -- Genesis Trousers
    21375, -- Stormcallers Leggings
    21368, -- Strikers Leggings
  },
  -- Qiraji Bindings of Dominance
  [20932] = {
    21388, -- Avengers Greaves
    21391, -- Avengers Pauldrons
    21338, -- Doomcallers Footwraps
    21335, -- Doomcallers Mantle
    21344, -- Enigma Boots
    21345, -- Enigma Shoulderpads
    21355, -- Genesis Boots
    21354, -- Genesis Shoulderpads
    21373, -- Stormcallers Footguards
    21376, -- Stormcallers Pauldrons
  },
  -- Husk of the Old God
  [20933] = {
    21334, -- Doomcallers Robes
    21343, -- Enigma Robes
    21357, -- Genesis Vest
    21351, -- Vestments of the Oracle
  },
  -- Head of Ossirian the Unscarred
  [21220] = {
    21504, -- Charm of the Shifting Sands
    21505, -- Choker of the Shifting Sands
    21506, -- Pendant of the Shifting Sands
    21507, -- Amulet of the Shifting Sands
  },
  -- Eye of C'Thun
  [21221] = {
    21712, -- Amulet of the Fallen God
    21710, -- Cloak of the Fallen God
    21709, -- Ring of the Fallen God
  },
  -- Imperial Qiraji Regalia
  [21237] = {
    21273, -- Blessed Qiraji Acolyte Staff
    21275, -- Blessed Qiraji Augur Staff
    21268, -- Blessed Qiraji War Hammer
  },
  -- Imperial Qiraji Armaments
  [21232] = {
    21242, -- Blessed Qiraji War Axe
    21272, -- Blessed Qiraji Musket
    21244, -- Blessed Qiraji Pugio
    21269, -- Blessed Qiraji Bulwark
  },
  -- Desecrated Breastplate
  [22349] = {
    22476, -- Bonescythe Breastplate
    22416, -- Dreadnaught Breastplate
  },
  -- Desecrated Tunic
  [22350] = {
    22436, -- Cryptstalker Tunic
    22488, -- Dreamwalker Tunic
    22464, -- Earthshatter Tunic
    22425, -- Redemption Tunic
  },
  -- Desecrated Robe
  [22351] = {
    22496, -- Frostfire Robe
    22504, -- Plagueheart Robe
    22512, -- Robe of Faith
  },
  -- Desecrated Legplates
  [22352] = {
    22477, -- Bonescythe Legplates
    22417, -- Dreadnaught Legplates
  },
  -- Desecrated Helmet
  [22353] = {
    22478, -- Bonescythe Helmet
    22418, -- Dreadnaught Helmet
  },
  -- Desecrated Pauldrons
  [22354] = {
    22479, -- Bonescythe Pauldrons
    22419, -- Dreadnaught Pauldrons
  },
  -- Desecrated Bracers
  [22355] = {
    22483, -- Bonescythe Bracers
    22423, -- Dreadnaught Bracers
  },
  -- Desecrated Waistguard
  [22356] = {
    22482, -- Bonescythe Waistguard
    22422, -- Dreadnaught Waistguard
  },
  -- Desecrated Gauntlets
  [22357] = {
    22481, -- Bonescythe Gauntlets
    22421, -- Dreadnaught Gauntlets
  },
  -- Desecrated Sabatons
  [22358] = {
    22480, -- Bonescythe Sabatons
    22420, -- Dreadnaught Sabatons
  },
  -- Desecrated Legguards
  [22359] = {
    22437, -- Cryptstalker Legguards
    22489, -- Dreamwalker Legguards
    22465, -- Earthshatter Legguards
    22427, -- Redemption Legguards
  },
  -- Desecrated Headpiece
  [22360] = {
    22438, -- Cryptstalker Headpiece
    22490, -- Dreamwalker Headpiece
    22466, -- Earthshatter Headpiece
    22428, -- Redemption Headpiece
  },
  -- Desecrated Spaulders
  [22361] = {
    22439, -- Cryptstalker Spaulders
    22491, -- Dreamwalker Spaulders
    22467, -- Earthshatter Spaulders
    22429, -- Redemption Spaulders
  },
  -- Desecrated Wristguards
  [22362] = {
    22443, -- Cryptstalker Wristguards
    22495, -- Dreamwalker Wristguards
    22471, -- Earthshatter Wristguards
    22424, -- Redemption Wristguards
  },
  -- Desecrated Girdle
  [22363] = {
    22442, -- Cryptstalker Girdle
    22494, -- Dreamwalker Girdle
    22470, -- Earthshatter Girdle
    22431, -- Redemption Girdle
  },
  -- Desecrated Handguards
  [22364] = {
    22441, -- Cryptstalker Handguards
    22493, -- Dreamwalker Handguards
    22469, -- Earthshatter Handguards
    22426, -- Redemption Handguards
  },
  -- Desecrated Boots
  [22365] = {
    22440, -- Cryptstalker Boots
    22492, -- Dreamwalker Boots
    22468, -- Earthshatter Boots
    22430, -- Redemption Boots
  },
  -- Desecrated Leggings
  [22366] = {
    22497, -- Frostfire Leggings
    22513, -- Leggings of Faith
    22505, -- Plagueheart Leggings
  },
  -- Desecrated Circlet
  [22367] = {
    22514, -- Circlet of Faith
    22498, -- Frostfire Circlet
    22506, -- Plagueheart Circlet
  },
  -- Desecrated Shoulderpads
  [22368] = {
    22499, -- Frostfire Shoulderpads
    22507, -- Plagueheart Shoulderpads
    22515, -- Shoulderpads of Faith
  },
  -- Desecrated Bindings
  [22369] = {
    22519, -- Bindings of Faith
    22503, -- Frostfire Bindings
    22511, -- Plagueheart Bindings
  },
  -- Desecrated Belt
  [22370] = {
    22518, -- Belt of Faith
    22502, -- Frostfire Belt
    22510, -- Plagueheart Belt
  },
  -- Desecrated Gloves
  [22371] = {
    22501, -- Frostfire Gloves
    22517, -- Gloves of Faith
    22509, -- Plagueheart Gloves
  },
  -- Desecrated Sandals
  [22372] = {
    22500, -- Frostfire Sandals
    22508, -- Plagueheart Sandals
    22516, -- Sandals of Faith
  },
  -- The Phylactery of Kel'Thuzad
  [22520] = {
    23206, -- Mark of the Champion
    23207, -- Mark of the Champion
  },
  [22726] = { -- Spinter of Atiesh
    22589, -- Atiesh, Greatstaff of the Guardian (Mage)
    22630, -- Atiesh, Greatstaff of the Guardian (Druid)
    22631, -- Atiesh, Greatstaff of the Guardian (Priest)
    22632, -- Atiesh, Greatstaff of the Guardian (Warlock)
  },
  [22733] = { -- Staff Head of Atiesh
    22589, -- Atiesh, Greatstaff of the Guardian (Mage)
    22630, -- Atiesh, Greatstaff of the Guardian (Druid)
    22631, -- Atiesh, Greatstaff of the Guardian (Priest)
    22632, -- Atiesh, Greatstaff of the Guardian (Warlock)
  },
  [22734] = { -- Base of Atiesh
    22589, -- Atiesh, Greatstaff of the Guardian (Mage)
    22630, -- Atiesh, Greatstaff of the Guardian (Druid)
    22631, -- Atiesh, Greatstaff of the Guardian (Priest)
    22632, -- Atiesh, Greatstaff of the Guardian (Warlock)
  },
}

self.CREATED_FROM = {}
for sourceItemID, items in pairs(self.CREATES) do
  for _, resultItemID in pairs(items) do
    if not self.CREATED_FROM[resultItemID] then
      self.CREATED_FROM[resultItemID] = {}
    end
    table.insert(self.CREATED_FROM[resultItemID], sourceItemID)
  end
end

function self:updateCreatedFrom()
  for _, playerInfo in pairs(self.PLAYER_INFO) do
    for _, slotData in pairs(playerInfo.slots) do
      local hasItem, hasAlternative, bestItems, optionalItems = unpack(slotData)
      for resultItemID, sourceItemIDTable in pairs(self.CREATED_FROM) do
        for _, sourceItemID in pairs(sourceItemIDTable) do
          self:updateCreatedFromTable(bestItems, sourceItemID, resultItemID)
          self:updateCreatedFromTable(optionalItems, sourceItemID, resultItemID)
        end
      end
    end
  end
end

function self:updateCreatedFromTable(tbl, sourceItemID, resultItemID)
  if self:tableIncludes(tbl, resultItemID) and not self:tableIncludes(tbl, sourceItemID) then
    table.insert(tbl, sourceItemID)
  end
end
