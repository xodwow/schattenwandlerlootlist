local self = SWLL

local initializedOnce = false

self.INDEX_TO_SLOT = {
  "MAINHAND",
  "OFFHAND",
  "TWOHAND",
  "RANGED",
  "HEAD",
  "NECK",
  "SHOULDER",
  "CLOAK",
  "CHEST",
  "WRIST",
  "HAND",
  "WAIST",
  "LEGS",
  "FEET",
  "FINGER1",
  "FINGER2",
  "TRINKET1",
  "TRINKET2",
}

self.SLOT_TO_INDEX = {}
for index, slot in pairs(self.INDEX_TO_SLOT) do
  self.SLOT_TO_INDEX[slot] = index
end

function self:initData()
  SchattenwandlerLootListData = SchattenwandlerLootListData or {}
  self.data = SchattenwandlerLootListData
  self.data.items = self.data.items or {}
  self.data.itemLinks = self.data.itemLinks or {}
  self.data.lastInspected = self.data.lastInspected or {}

  self:importData()
  initializedOnce = true
end

function self:getItemLink(itemID)
  if self.data.itemLinks[itemID] then
    return self.data.itemLinks[itemID]
  end
  local itemName, itemLink = GetItemInfo(itemID)
  self.data.itemLinks[itemID] = itemLink
  return itemLink or itemID
end

function self:hasEquipped(playerName, itemID)
  return self.data.items[playerName] and
         self.data.items[playerName][itemID] and
         self.data.items[playerName][itemID][1] or
         false
end

function self:tableIncludes(tbl, value)
  for k, v in pairs(tbl) do
    if v == value then
      return true
    end
  end
  return false
end

function self:needsForMainList(playerName, itemID)
  local slots = self.PLAYER_INFO[playerName] and self.PLAYER_INFO[playerName].slots
  if not slots then
    return false
  end
  for slotIndex, slotData in pairs(slots) do
    local hasItem, hasAlternative, bestItems, optionalItems = unpack(slotData)
    local slotName = self.INDEX_TO_SLOT[slotIndex]
    local needs = self:needsSlot(playerName, slotName) and self:tableIncludes(bestItems, itemID)
    if needs then
      return true
    end
  end
  return false
end

function self:needsForAlternativeList(playerName, itemID)
  local slots = self.PLAYER_INFO[playerName] and self.PLAYER_INFO[playerName].slots
  if not slots then
    return false
  end
  for slotIndex, slotData in pairs(slots) do
    local hasItem, hasAlternative, bestItems, optionalItems = unpack(slotData)
    local slotName = self.INDEX_TO_SLOT[slotIndex]
    local needs = self:needsSlot(playerName, slotName) and self:tableIncludes(optionalItems, itemID)
    if needs then
      return true
    end
  end
  return false
end

function self:needsSlot(playerName, slot)
  if slot == "TWOHAND" then
    if not self:needsExactSlot(playerName, "MAINHAND") and not self:needsExactSlot(playerName, "MAINHAND") then
      return false
    end
  end
  if slot == "MAINHAND" or slot == "OFFHAND" then
    if not self:needsExactSlot(playerName, "TWOHAND") then
      return false
    end
  end
  return self:needsExactSlot(playerName, slot)
end

function self:needsExactSlot(playerName, slot)
  if not slot then return false end
  local slotsForPlayer = self.playerHasItemInSlotTable[playerName]
  return not slotsForPlayer or slotsForPlayer[slot] == self.SLOT.EMPTY
end

function self:exactSlotState(playerName, slot)
  local slotsForPlayer = self.playerHasItemInSlotTable[playerName]
  return slotsForPlayer and slotsForPlayer[slot] or self.SLOT.EMPTY
end

function self:importData()
  if not initializedOnce then
    self:printf("Imported loot sheet data from %s", self.IMPORT.timestamp)
  end

  if self.data.import and self.data.import.timestamp > self.IMPORT.timestamp then
    self.IMPORT = self.data.import
  else
    self.data.import = nil
  end

  self.PLAYER_INFO = {}
  for _, playerInfo in pairs(self.IMPORT.players) do
    self.PLAYER_INFO[playerInfo.name] = playerInfo
  end

  self:updateCreatedFrom()

  -- init with data export
  self.playerHasItemInSlotTable = {}
  for _, playerInfo in pairs(self.IMPORT.players) do
    local slots = {}
    for slotIndex, slotData in pairs(playerInfo.slots) do
      local hasItem, hasAlternative, bestItems, optionalItems = unpack(slotData)
      slots[self.INDEX_TO_SLOT[slotIndex]] = hasItem
    end
    self.playerHasItemInSlotTable[playerInfo.name] = slots
  end

  -- update with local DB
  for _, playerInfo in pairs(self.IMPORT.players) do
    for slotIndex, slotData in pairs(playerInfo.slots) do
      local slotName = self.INDEX_TO_SLOT[slotIndex]
      local hasItem, hasAlternative, bestItems, optionalItems = unpack(slotData)
      if self.playerHasItemInSlotTable[playerInfo.name][slotName] == self.SLOT.EMPTY then
        for _, itemID in pairs(bestItems) do
          if self:hasEquipped(playerInfo.name, itemID) then
            local link = self:getItemLink(itemID)
            local formattedName = self:classColored(playerInfo.name, playerInfo.class, false)
            if not initializedOnce and self.printWarnings then
              self:printf("Warning, %s has no X for %s (%s)", formattedName, link, slotName)
            end
            self.playerHasItemInSlotTable[playerInfo.name][slotName] = true
            break
          end
        end
      end
    end
  end

  self.playerItemCounts = {}
  for playerName, slots in pairs(self.playerHasItemInSlotTable) do
    self.playerItemCounts[playerName] = 0
    for slot, value in pairs(slots) do
      if value == self.SLOT.FILLED then
        self.playerItemCounts[playerName] = self.playerItemCounts[playerName] + 1
      end
    end

    -- MAINHAND counts as 1 item
    -- OFFHAND counts as 1 item
    -- MAINHAND+OFFHAND counts as 1 item
    -- TWOHAND counts as 1 item
    -- TWOHAND+MAINHAND+OFFHAND counts as 2 items
    -- TWOHAND+MAINHAND counts as 2 items
    -- TWOHAND+OFFHAND counts as 2 items

    -- to implement it, the count is reduced afterwards to account for the MAINHAND+OFFHAND = 1 item special case
    if self.playerHasItemInSlotTable[playerName].MAINHAND == self.SLOT.FILLED
            and self.playerHasItemInSlotTable[playerName].OFFHAND == self.SLOT.FILLED then
      self.playerItemCounts[playerName] = self.playerItemCounts[playerName] - 1
    end
  end
end

function self:playerItemCount(playerName)
  return self.playerItemCounts[playerName] or 0
end

function self:saveEquipped(playerName, playerClass, itemID)
  if not self.data.items[playerName] then self.data.items[playerName] = {} end
  if not self.data.items[playerName][itemID] then self.data.items[playerName][itemID] = {} end

  if self:hasEquipped(playerName, itemID) then return end

  self.data.lastInspected[playerName] = self.data.lastInspected[playerName] and (time() - 300)
  local items = self.data.items[playerName][itemID]

  local dateStr = date("%Y-%m-%dT%T")
  table.insert(self.data.items[playerName][itemID], dateStr .. " (equipped)")
  self:importData()
  self:printLooted(playerName, playerClass, itemID, "equipped")
end

function self:printLooted(playerName, playerClass, itemID, suffix)
  local item = Item:CreateFromItemID(itemID)
  item:ContinueOnItemLoad(function()
    self:printf("%s %s %s", self:classColored(playerName, playerClass), suffix, item:GetItemLink())
    self:getItemLink(itemID) -- to store it
  end)
end
